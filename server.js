

var config = require('./config/env');
var app = require('./config/express');

var server = app.listen(config.port, function(err) {
    console.log('Running in: ', config.env);
    console.log('Server started on',  config.port);
 });