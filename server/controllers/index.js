/*
 * 20th Nov, 2016
 * 
 * @module: 
 * Module to Export and open the controllers 
 * by standard names. 
 *
 * @author: Ankit Singh
**/

exports.User = require('./user.js');
exports.Role = require('./role.js');
exports.Permission = require('./permission.js');