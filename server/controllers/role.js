
var fileName = '../models/role.json';
var Role = require(fileName);
var _ = require('underscore')
var fs = require('fs');


module.exports = {

	get: function(req, res, next) {

	},

	getPermissions: function(roleIds) {
		var roles = Role.values; 
		var permissions = []

		for (var i = roles.length - 1; i >= 0; i--) {
			for (var j = roleIds.length - 1; j >= 0; j--) {
				if (roles[i].id == roleIds[j]) {
					permissions.push(roles[i].permissions)
				}
			}
		}
		return _.flatten(permissions)
	},

	hasPermission: function(roles, permissionId) {
		var permissions = this.getPermissions(roles)

		for (var i = permissions.length - 1; i >= 0; i--) {
			if (permissions[i] == permissionId) {
				return true
			}
		}
		return false
	},


	update: function(req, res, next) {

		var roleId = req.params.roleId;
		Role.values[roleId] = req.body.permissions
		fs.writeFile(fileName, JSON.stringify(Role), function (err) {
		  if (err) return res.send(err);
		  console.log('writing to ' + fileName);
		  return res.send({ status: true, message: 'Successfully Updated'})
		});
	},


	find: function(roleIds) {
		var permissions = []
		var roles = Role.values; 
		for (var i = roles.length - 1; i >= 0; i--) {
			for (var j = roleIds.length - 1; j >= 0; j--) {
				if(roles[i].id == roleIds[j]) {
					permissions.push(roles[i].permissions)
				}
			}
		}
		return permissions
	}
}