

var User = require('../models/user.json');
var RoleController = require('./role.js');
var PermissionController = require('./permission.js')
var _ = require('underscore')

module.exports = {

	get: function(req, res, next) {
		var userId = req.params.userId;
		var users = User.values; 

		for (var i = users.length - 1; i >= 0; i--) {
			if (users[i].id == userId) {
				return res.send({
					status: true,
					message: "user Id exists",
					permissions: PermissionController.find(_.flatten(RoleController.find(users[i].roles)))
				});
			}
		}

		res.send({
			status: false,
			message: "No permission found"
		});
	}

}