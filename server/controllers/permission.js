

var fileName = '../models/permission.json';
var Permission = require(fileName);
var User = require('../models/user.json')
var UserController = require('./user.js')
var RoleController = require('./role.js')

module.exports = {

	get: function(req, res, next) {

	},

	check: function(req, res, next) {

		var userId = req.query.userid
		var permissionId = req.query.permissionid
		var roles = []

		var users = User.values; 
		for (var i = users.length - 1; i >= 0; i--) {
			if (users[i].id == userId) {
				roles = users[i].roles
				break;
			}
		}
		res.send({
			status: true,
			hasPermission: RoleController.hasPermission(roles,permissionId),
		});

	},


	find: function(permissionIds) {
		var permissionNames = []
		var permissions = Permission.values; 
		for (var i = permissions.length - 1; i >= 0; i--) {
			for (var j = permissionIds.length - 1; j >= 0; j--) {
				if(permissions[i].id == permissionIds[j]) {
					permissionNames.push(permissions[i].name)
				}
			}
		}
		return permissionNames
	},

	delete: function(req, res, next) {
		var permisionId = req.params.permissionId;

		res.send({
			status: false,
			message: "Deletion not working. Database would be a better option to do such task instead of modifying a JSON file."
		})

	}


}