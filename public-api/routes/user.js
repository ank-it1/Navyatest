

var Controller = require('../../server/controllers');
var	router = require('express').Router()

router.get('/hi', function(req, res, next) {
	console.log('Well You are now inside');
	res.send('OK');
});

// 1. Given user, return list of names of permissions that this user is entitled to.     url: http://<server>/user/<userid> (GET method)
router.route('/users/:userId')
	.get(Controller.User.get);
	
// 2. Given user and permission, return boolean if entitled or not. url : http://<server>/checkpermission/?userid=<user_id>&permissionid=<permission_id> (GET Method)
router.route('/checkpermission')
	.get(Controller.Permission.check);

// 3. Modify permissions of a role ex: http://<server>/roles/<roleid> POST_PARAM:{"permissions":["perm5"]} (POST Method)
router.route('/roles/:roleId')
	.post(Controller.Role.update);

// 4. Delete a permission ex: http://<server>/permissions/<permission_id> (DELETE Method).
router.route('/permissions/:permissionId')
	.delete(Controller.Permission.delete);


module.exports = router;