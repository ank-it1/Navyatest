

var express = require('express');
var bodyParser = require('body-parser');
var Routes = require('../public-api');
var config = require('./env');
var APIError = require('../server/helpers/APIError.js');
var httpStatus = require('http-status')
var app = express();


// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/', Routes.APIRoutes);


// if error is not an instanceOf APIError, convert it.
app.use(function(err, req, res, next) {
  if (err instanceof expressValidation.ValidationError) {
    // validation error contains errors which is an array of error each containing message[]
    const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ');
    const error = new APIError(unifiedErrorMessage, err.status, true);
    return next(error);
  } else if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic);
    return next(apiError);
  }
  else if (!(err instanceof Error)) {
    const apiError = new APIError(err.message, 400, true);
    return next(apiError);
  }
  return next(err);
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new APIError('API not found', httpStatus.NOT_FOUND);
  return next(err);
});


// error handler, send stacktrace only during development
app.use( function(err, req, res, next) {
  res.status(err.status).json({
    status: 0,
    message: err.isPublic ? err.message : httpStatus[err.status],
    stack: config.env === 'development' ? err.stack : {}
  })
});


module.exports = app;